from django.db import models

class Deck(models.Model):
    '''
    The players deck
    '''
    total_cards = models.PositiveBigIntegerField()


class PlayerCharacter(models.model):
    name = models.CharField(max_length=50)
    totalhp = models.PositiveIntegerField()
    currenthp = models.PositiveIntegerField()
    energy = models.PositiveBigIntegerField()

    deck = models.ForeignKey(
        Deck,
        related_name="player_deck",
        on_delete=models.CASCADE
    )



class AttackCard(models.Model):
    '''
    attack type cards
    '''
    name = models.CharField(max_length=50)
    cost = models.PositiveIntegerField()
    damage = models.CharField(max_length=50)
    description = models.CharField(max_length=150)

class blockCard(models.Model):
    '''
    block type cards
    '''
    name = models.CharField(max_length=50)
    cost = models.PositiveIntegerField()
    block = models.CharField(max_length=50)
    description = models.CharField(max_length=150)

class MiscCard(models.Model):
    '''
    unquie non block/attack type cards
    '''
    name = models.CharField(max_length=50)
    cost = models.PositiveIntegerField()
    effect = models.CharField(max_length=50)
    description = models.CharField(max_length=150)

class Enemy(models.Model):
    '''
    All basic enemies types
    '''
    name = models.CharField(max_length=50)
    totalhp = models.PositiveIntegerField()
    currenthp = models.PositiveIntegerField()
    attack = models.PositiveIntegerField()
